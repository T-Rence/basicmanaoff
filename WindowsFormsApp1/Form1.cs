﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "") 
            {
                MessageBox.Show("Une information est manquante !");
            }
            else
            {
                try
                {

                    this.dataGridView1.Rows.Add(textBox1.Text, textBox2.Text, textBox3.Text, dateTimePicker1.Value.Date);
                    this.textBox1.Text = "";
                    this.textBox2.Clear();
                    this.textBox3.Clear();
                    
                }
            
                 catch (FormatException)
                {
                    MessageBox.Show("Test");
                }
            }
        }

        private void btnSupp_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Voulez-vous supprimer cette ligne?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.OK)
                {
                    int index = this.dataGridView1.CurrentRow.Index;
                    this.dataGridView1.Rows.RemoveAt(index);
                    MessageBox.Show("Supprimer avec succès");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Aucune tâche sélectionnée");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.groupBox1.Visible = false;
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
        int index;
        private void btnMod_Click(object sender, EventArgs e)
        {
            int trouve = 0;
           
            for (int i = 0; i < this.dataGridView1.Rows.Count -1; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[0].Value.ToString()== txtID.Text)
                {
                    index = i;
                    trouve = 1;
                }
            }
            if (trouve == 0)
                MessageBox.Show("Tâche inexistante");
            else
                this.groupBox1.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                    this.dataGridView1.Rows[index].Cells[0].Value = NewID.Text;
                    this.dataGridView1.Rows[index].Cells[1].Value = NewName.Text;
                    this.dataGridView1.Rows[index].Cells[2].Value = NewDesc.Text;
                    this.dataGridView1.Rows[index].Cells[3].Value = dateTimePicker2.Value.Date;
                    this.NewID.Clear();
                    this.NewName.Clear();
                    this.NewDesc.Clear();                    
                    this.groupBox1.Visible = false;
                    this.txtID.Clear();
                

        }

        private void NewID_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void btnRech_Click(object sender, EventArgs e)
        {
            int cmp = 0;
            for (int i = 0; i < this.dataGridView1.Rows.Count-1; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[0].Value.ToString()==this.SearchID.Text) 
                {
                    MessageBox.Show("Nom : " + this.dataGridView1.Rows[i].Cells[1].Value + "\nDescription : " + this.dataGridView1.Rows[i].Cells[2].Value + "\nDate de fin: "+this.dataGridView1.Rows[i].Cells[3].Value);
                    cmp = 1;
                }
            }
            if (cmp == 0)
                MessageBox.Show("Tâche inexistante");
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }
        public void AddFileToDGV()
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.InitialDirectory = "Z:\\Documents\basic_task_manager\\WindowsFormsApp1";
            openFile.Filter = "txt files (*.txt)|*.txt";
            openFile.FilterIndex = 2;
            openFile.RestoreDirectory = true;
            try
            {
                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    string file = openFile.FileName;
                    StreamReader sr = new StreamReader(file);

                    /* gets all the lines of the csv */
                    string[] str = File.ReadAllLines(file);

                    /* creates a data table*/
                    DataTable dt = new DataTable();

                    /* gets the column headers from the first line*/
                    string[] temp = str[0].Split(',');

                    /* creates columns of the gridview base on what is stored in the temp array */
                    

                    int count = 0;
                    /* retrieves the rows after the first row and adds it to the datatable */
                    for (int i = 1; i < str.Length; i++)
                    {
                        if (str[i].Contains('"'))
                        {
                            count++;

                            if (str[i].StartsWith("") && str[i].EndsWith(""))
                            {
                                string[] t = str[i].Split('"');

                                // count++;
                                // MessageBox.Show(t[i]);
                            }
                            //str[i + 1];

                        }
                        else
                        {
                            string[] t = str[i].Split(',');
                            dataGridView1.Rows.Add(t);
                        }

                    }
                    MessageBox.Show("Number of Rows with a Quote In it is: " + count);

                    /* assigns the data grid view a data source based on what is stored in dt  */
                    dataGridView1.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: The CSV you selected could not be loaded" + ex.Message);
            }
        }
    }
}
